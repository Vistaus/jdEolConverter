<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="23"/>
        <source>jdEolConverter converts the End of Line of all files in the given Directory. Select a Directory and click OK to get started.</source>
        <translation>jdEolConverter converteert regeleinden van alle bestanden in de gekozen map. Kies een map en klik op ‘Oké’ om het proces te starten.</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="38"/>
        <source>Directory:</source>
        <translation>Map:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="48"/>
        <source>Browse</source>
        <translation>Bladeren…</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="57"/>
        <source>End of Line</source>
        <translation>Regeleinde</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="93"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="99"/>
        <source>Convert only files which name matches this RegEx</source>
        <translation>Alleen bestanden converteren die overeenkomen met reguliere uitdrukking</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="108"/>
        <source>RegEx:</source>
        <translation>Reguliere uitdrukking:</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="120"/>
        <source>Skip hidden files</source>
        <translation>Verborgen bestanden overslaan</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="127"/>
        <source>Recursive</source>
        <translation>Recursief</translation>
    </message>
</context>
</TS>
